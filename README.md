# ITT3-PCB-Simon_bjerre

## Introduction
This project is created to contain the project done in relation to the PCB production subject on third semester of the IT technology education.
Final product for this project is a physical PCB board populated with components, documentation will include descriptions of the design and learning process of designing a PCB from idea to final product.
This project specifically will be based around creating a carrierboard for the Raspberry pi Pico, which allows it to access the internet and utilize TCP/IP based protocols to share data for IoT porpuses.

## Documentation
<p align="center">
  <a href="https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-simon_bjerre/-/tree/main/documentation"><img src="https://img.shields.io/badge/-PCB%20documentation-blueviolet?style=for-the-badge" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>

## Schematics
<p align="center">
  <a href="https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-simon_bjerre/-/tree/main/schematics/KiCad"><img src="https://img.shields.io/badge/-PCB%20schematics-blueviolet?style=for-the-badge" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>

## Custom libraries
<p align="center">
  <a href="https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-simon_bjerre/-/tree/main/schematics/KiCad/custom_libs"><img src="https://img.shields.io/badge/-Custom%20libraries-blueviolet?style=for-the-badge" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>

## Datasheets
<p align="center">
  <a href="https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-simon_bjerre/-/tree/main/datasheets"><img src="https://img.shields.io/badge/-PCB%20Datasheets-blueviolet?style=for-the-badge" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>


## Information
<p align="center">
  <a href="mailto:gismosif@gmail.com?subject=Hello%20Simon"><img src="https://img.shields.io/badge/gmail-%23D14836.svg?style=for-the-badge&logo=gmail&logoColor=white" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://gitlab.com/SimonBjerre"><img src="https://img.shields.io/badge/-Gitlab%20profile-lightgrey?style=for-the-badge&logo=GitLab&logoColor=white" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://www.linkedin.com/in/simon-b-96b1551bb/"><img src="https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>

