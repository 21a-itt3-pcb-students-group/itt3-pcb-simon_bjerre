import board
import busio
from digitalio import DigitalInOut
from adafruit_esp32spi import adafruit_esp32spi
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from adafruit_esp32spi import adafruit_esp32spi_wifimanager
from adafruit_io.adafruit_io import IO_MQTT
from time import sleep
try:
    from secrets import secrets
except ImportError:
    print("Could not locate secrets.py")
    exit(0)

# setting up connections for the airlift
esp32_cs = DigitalInOut(board.GP13)
esp32_ready = DigitalInOut(board.GP14)
esp32_reset = DigitalInOut(board.GP15)
spi = busio.SPI(board.GP10, board.GP11, board.GP12)
airlift = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)

#Setting up wifi 
wifi = adafruit_esp32spi_wifimanager.ESPSPI_WiFiManager(airlift, secrets)
wifi.connect()
print(f'checking connectivity:{airlift.ping("google.com")}')

# Setting up for MQTT
MQTT.set_socket(socket, airlift)
mqtt_client = MQTT.MQTT(broker="https://test.mosquitto.org/")
io = IO_MQTT(mqtt_client)
io.connect()

#Read--> publish loop
While True:
	temp = 1 #temp variable can be filled from sensor readings.
	io.publish("temperature",temp)
	print("Published")
	sleep(5)