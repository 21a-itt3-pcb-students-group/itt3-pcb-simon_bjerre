## Week 34:
### Activities
* Set up Gitlab project
* Research possible projects
* draw block diagram 
* Check student license is still working for orcad programs
* Ordering components

### Learning outcome
Set up gtilab project in the PCB production group
Block diagram created for high level overview
Innital knowledge on how the PCB's are produced by the manifactures
Genereated idea for project in the course
components ordered

### Ressources
Exercises week 34: https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww34

## Week 35

### Activities
* Nordcad beginner course in schematic, simulation and PCB design
* Draw schematic of whole circuit and subcurcuit
* Simulate and document outputs from Capture

### Learning outcome
From the capture course i learned how to create custom parts to be used in my electrical circuits designed in the capture program.
From the course in simulation i learned how to use the PSpice orcad program to simulate and document the behaviour of a given circuit.

### Ressources
Courses from nordcad
https://www.nordcad.eu/student-forum/beginners-course/

capture tutorials
https://resources.orcad.com/orcad-capture-tutorials 


## Week 36
### Activities
* SMT/SMD component research
* Designing custom footprints.

### Learning outcome
How to do research and documentation on components for PCB's, also how said components look, as looking at a datasheet isn't always representative of how a component looks.
Finding key relevant information in relation to PCB use of a component
Learned how to import/create custom footprints 

### Ressources
Weekly exercises: https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww36

OrCAD 17.2 PCB Design Tutorial - 04 - Capture: Preparing for Manufacturehttps://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4

Gather footprints for your circuit (see ressources in comments) Instructions https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6

Ultralibrarian: https://www.ultralibrarian.com/


## Week 37
### Activities
* Creation of netlist for project
* Design rule check
* Setting up board outline

### Learning outcome

### Ressources
Weekly exercises: https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37




## Week 43
### Activities
* Moved project to KiCad due to student licens for orcad expiring 
* Concepts from Orcad easy transferable and the system is rebuilt quickly.
* Innital design on the PCB layout begun in KiCad
* Considering moving wifi module below RPI for easy wiring down the middle of the RPI
* Learned how to use electrical rule check to see if all the pins are connected


## Week 44
### Activities
* Learned that when working with many connections, the rats-nest tool can be helpfull
* Reduced ammount of pins run to the middle from the RPI
* Also moved to both sides of RPI instead of just having them in the middle, for easier routing
* Added costum silkscreen figure


## Week 45
### Activities
* Designrule checks
* Design for manufacturing
* Exporting to gerber/drill files
* Ordering components and board


## Week 46
### Activities
* Documentation
* Start of report writing - document and template created
* testplan created


## Week 47
### Activities
* Documentation
* continued report writing


## Week 48
### Activities
* PCB assembly
* Evaluation of project
* Continued report writing





