EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x20 RPI_RIGHT
U 1 1 6180FEB4
P 4000 3500
F 0 "RPI_RIGHT" H 3918 4617 50  0000 C CNN
F 1 "Conn_01x20" H 3918 4526 50  0000 C CNN
F 2 "" H 4000 3500 50  0001 C CNN
F 3 "~" H 4000 3500 50  0001 C CNN
	1    4000 3500
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x12 AIRLIFT_RIGHT
U 1 1 61811928
P 8300 3400
F 0 "AIRLIFT_RIGHT" H 8218 4117 50  0000 C CNN
F 1 "Conn_01x12" H 8218 4026 50  0000 C CNN
F 2 "" H 8300 3400 50  0001 C CNN
F 3 "~" H 8300 3400 50  0001 C CNN
	1    8300 3400
	-1   0    0    -1  
$EndComp
Text Notes 3600 2700 0    50   ~ 0
USB
Text Notes 7600 4200 0    50   ~ 0
PROSSESOR
Text GLabel 4200 2700 2    50   Input ~ 0
VSYS
Text GLabel 4200 2800 2    50   Input ~ 0
SHARED_GND
Text GLabel 8500 3100 2    50   Input ~ 0
VSYS
Text GLabel 7300 3200 0    50   Input ~ 0
SHARED_GND
$Comp
L Connector_Generic:Conn_01x16 AIRLIFT_LEFT
U 1 1 61822077
P 7500 3600
F 0 "AIRLIFT_LEFT" H 7580 3592 50  0000 L CNN
F 1 "Conn_01x16" H 7580 3501 50  0000 L CNN
F 2 "" H 7500 3600 50  0001 C CNN
F 3 "~" H 7500 3600 50  0001 C CNN
	1    7500 3600
	1    0    0    -1  
$EndComp
Text GLabel 4200 2600 2    50   Input ~ 0
VBUS
Text GLabel 4200 2900 2    50   Input ~ 0
3V3_EN
Text GLabel 4200 3000 2    50   Input ~ 0
3V3_OUT
Text GLabel 4200 3100 2    50   Input ~ 0
ADC_VREF
Text GLabel 4200 3200 2    50   Input ~ 0
GP28
Text GLabel 4200 3300 2    50   Input ~ 0
GND5
Text GLabel 4200 3400 2    50   Input ~ 0
GP27
Text GLabel 4200 3500 2    50   Input ~ 0
GP26
Text GLabel 4200 3600 2    50   Input ~ 0
RUN
Text GLabel 4200 3700 2    50   Input ~ 0
GP22
Text GLabel 4200 3800 2    50   Input ~ 0
GND6
Text GLabel 4200 3900 2    50   Input ~ 0
GP21
Text GLabel 4200 4000 2    50   Input ~ 0
GP20
Text GLabel 4200 4100 2    50   Input ~ 0
GP19
Text GLabel 4200 4200 2    50   Input ~ 0
GP18
Text GLabel 4200 4300 2    50   Input ~ 0
GND7
Text GLabel 4200 4400 2    50   Input ~ 0
GP17
Text GLabel 4200 4500 2    50   Input ~ 0
GP16
$Comp
L Connector_Generic:Conn_01x20 RPI_LEFT
U 1 1 61827E07
P 3400 3500
F 0 "RPI_LEFT" H 3480 3492 50  0000 L CNN
F 1 "Conn_01x20" H 3480 3401 50  0000 L CNN
F 2 "" H 3400 3500 50  0001 C CNN
F 3 "~" H 3400 3500 50  0001 C CNN
	1    3400 3500
	1    0    0    -1  
$EndComp
Text GLabel 3200 2600 0    50   Input ~ 0
GP0
Text GLabel 3200 2700 0    50   Input ~ 0
GP1
Text GLabel 3200 2800 0    50   Input ~ 0
GND1
Text GLabel 3200 2900 0    50   Input ~ 0
GP2
Text GLabel 3200 3000 0    50   Input ~ 0
GP3
Text GLabel 3200 3100 0    50   Input ~ 0
GP4
Text GLabel 3200 3200 0    50   Input ~ 0
GP5
Text GLabel 3200 3300 0    50   Input ~ 0
GND2
Text GLabel 3200 3400 0    50   Input ~ 0
GP6
Text GLabel 3200 3500 0    50   Input ~ 0
GP7
Text GLabel 3200 3600 0    50   Input ~ 0
GP8
Text GLabel 3200 3700 0    50   Input ~ 0
GP9
Text GLabel 3200 3800 0    50   Input ~ 0
GND3
Text GLabel 3200 3900 0    50   Input ~ 0
GP10
Text GLabel 3200 4000 0    50   Input ~ 0
GP11
Text GLabel 3200 4100 0    50   Input ~ 0
GP12
Text GLabel 3200 4200 0    50   Input ~ 0
GP13
Text GLabel 3200 4300 0    50   Input ~ 0
GND4
Text GLabel 3200 4400 0    50   Input ~ 0
GP14
Text GLabel 3200 4500 0    50   Input ~ 0
GP15
Text GLabel 7300 4100 0    50   Input ~ 0
GP12
Text GLabel 7300 4000 0    50   Input ~ 0
GP11
Text GLabel 7300 3900 0    50   Input ~ 0
GP10
Text GLabel 8500 3200 2    50   Input ~ 0
GP13
Text GLabel 8500 3300 2    50   Input ~ 0
GP15
Text GLabel 8500 3400 2    50   Input ~ 0
GP14
$EndSCHEMATC
