EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x20 RPI_RIGHT1
U 1 1 6180FEB4
P 4700 3500
F 0 "RPI_RIGHT1" H 4618 4617 50  0000 C CNN
F 1 "Conn_01x20" H 4618 4526 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x20_P2.54mm_Vertical" H 4700 3500 50  0001 C CNN
F 3 "~" H 4700 3500 50  0001 C CNN
	1    4700 3500
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x12 AIRLIFT_RIGHT1
U 1 1 61811928
P 9300 3500
F 0 "AIRLIFT_RIGHT1" H 9218 4217 50  0000 C CNN
F 1 "Conn_01x12" H 9218 4126 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 9300 3500 50  0001 C CNN
F 3 "~" H 9300 3500 50  0001 C CNN
	1    9300 3500
	-1   0    0    -1  
$EndComp
Text Notes 4300 2700 0    50   ~ 0
USB
Text Notes 8600 4300 0    50   ~ 0
PROSSESOR
Text GLabel 4900 2700 2    50   Input ~ 0
VSYS
Text GLabel 4900 2800 2    50   Input ~ 0
GND
Text GLabel 9500 3200 2    50   Input ~ 0
VSYS
Text GLabel 8300 3300 0    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_01x16 AIRLIFT_LEFT1
U 1 1 61822077
P 8500 3700
F 0 "AIRLIFT_LEFT1" H 8580 3692 50  0000 L CNN
F 1 "Conn_01x16" H 8580 3601 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical" H 8500 3700 50  0001 C CNN
F 3 "~" H 8500 3700 50  0001 C CNN
	1    8500 3700
	1    0    0    -1  
$EndComp
Text GLabel 4900 2600 2    50   Input ~ 0
VBUS
Text GLabel 4900 2900 2    50   Input ~ 0
3V3_EN
Text GLabel 4900 3000 2    50   Input ~ 0
3V3_OUT
Text GLabel 4900 3100 2    50   Input ~ 0
ADC_VREF
Text GLabel 4900 3200 2    50   Input ~ 0
GP28
Text GLabel 4900 3300 2    50   Input ~ 0
GND
Text GLabel 4900 3400 2    50   Input ~ 0
GP27
Text GLabel 4900 3500 2    50   Input ~ 0
GP26
Text GLabel 4900 3600 2    50   Input ~ 0
RUN
Text GLabel 4900 3700 2    50   Input ~ 0
GP22
Text GLabel 4900 3800 2    50   Input ~ 0
GND
Text GLabel 4900 3900 2    50   Input ~ 0
GP21
Text GLabel 4900 4000 2    50   Input ~ 0
GP20
Text GLabel 4900 4100 2    50   Input ~ 0
GP19
Text GLabel 4900 4200 2    50   Input ~ 0
GP18
Text GLabel 4900 4300 2    50   Input ~ 0
GND
Text GLabel 4900 4400 2    50   Input ~ 0
GP17
Text GLabel 4900 4500 2    50   Input ~ 0
GP16
$Comp
L Connector_Generic:Conn_01x20 RPI_LEFT1
U 1 1 61827E07
P 4100 3500
F 0 "RPI_LEFT1" H 4180 3492 50  0000 L CNN
F 1 "Conn_01x20" H 4180 3401 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x20_P2.54mm_Vertical" H 4100 3500 50  0001 C CNN
F 3 "~" H 4100 3500 50  0001 C CNN
	1    4100 3500
	1    0    0    -1  
$EndComp
Text GLabel 3900 2600 0    50   Input ~ 0
GP0
Text GLabel 3900 2700 0    50   Input ~ 0
GP1
Text GLabel 3900 2800 0    50   Input ~ 0
GND
Text GLabel 3900 2900 0    50   Input ~ 0
GP2
Text GLabel 3900 3000 0    50   Input ~ 0
GP3
Text GLabel 3900 3100 0    50   Input ~ 0
GP4
Text GLabel 3900 3200 0    50   Input ~ 0
GP5
Text GLabel 3900 3300 0    50   Input ~ 0
GND
Text GLabel 3900 3400 0    50   Input ~ 0
GP6
Text GLabel 3900 3500 0    50   Input ~ 0
GP7
Text GLabel 3900 3600 0    50   Input ~ 0
GP8
Text GLabel 3900 3700 0    50   Input ~ 0
GP9
Text GLabel 3900 3800 0    50   Input ~ 0
GND
Text GLabel 3900 3900 0    50   Input ~ 0
GP10
Text GLabel 3900 4000 0    50   Input ~ 0
GP11
Text GLabel 3900 4100 0    50   Input ~ 0
GP12
Text GLabel 3900 4200 0    50   Input ~ 0
GP13
Text GLabel 3900 4300 0    50   Input ~ 0
GND
Text GLabel 3900 4400 0    50   Input ~ 0
GP14
Text GLabel 3900 4500 0    50   Input ~ 0
GP15
Text GLabel 8300 4200 0    50   Input ~ 0
GP12
Text GLabel 8300 4100 0    50   Input ~ 0
GP11
Text GLabel 8300 4000 0    50   Input ~ 0
GP10
Text GLabel 9500 3300 2    50   Input ~ 0
GP13
Text GLabel 9500 3400 2    50   Input ~ 0
GP15
Text GLabel 9500 3500 2    50   Input ~ 0
GP14
$Comp
L Connector_Generic:Conn_01x16 PIN_LEFT1
U 1 1 6180F8D6
P 2800 3500
F 0 "PIN_LEFT1" H 2880 3492 50  0000 L CNN
F 1 "Conn_01x16" H 2880 3401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2800 3500 50  0001 C CNN
F 3 "~" H 2800 3500 50  0001 C CNN
	1    2800 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 PIN_RIGHT1
U 1 1 618129A5
P 5600 3500
F 0 "PIN_RIGHT1" H 5518 4417 50  0000 C CNN
F 1 "Conn_01x16" H 5518 4326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 5600 3500 50  0001 C CNN
F 3 "~" H 5600 3500 50  0001 C CNN
	1    5600 3500
	-1   0    0    -1  
$EndComp
Text GLabel 2600 2800 0    50   Input ~ 0
GP0
Text GLabel 2600 2900 0    50   Input ~ 0
GP1
Text GLabel 2600 3000 0    50   Input ~ 0
GND
Text GLabel 2600 3100 0    50   Input ~ 0
GP2
Text GLabel 2600 3200 0    50   Input ~ 0
GP3
Text GLabel 2600 3300 0    50   Input ~ 0
GP4
Text GLabel 2600 3400 0    50   Input ~ 0
GP5
Text GLabel 2600 3500 0    50   Input ~ 0
GND
Text GLabel 2600 3600 0    50   Input ~ 0
GP6
Text GLabel 2600 3700 0    50   Input ~ 0
GP7
Text GLabel 2600 3800 0    50   Input ~ 0
GP8
Text GLabel 2600 3900 0    50   Input ~ 0
GP9
Text GLabel 2600 4000 0    50   Input ~ 0
GND
Text GLabel 5800 2800 2    50   Input ~ 0
VBUS
Text GLabel 5800 2900 2    50   Input ~ 0
3V3_EN
Text GLabel 5800 3000 2    50   Input ~ 0
3V3_OUT
Text GLabel 5800 3100 2    50   Input ~ 0
ADC_VREF
Text GLabel 2600 4100 0    50   Input ~ 0
GND
Text GLabel 2600 4200 0    50   Input ~ 0
GP17
Text GLabel 5800 4300 2    50   Input ~ 0
GND
Text GLabel 5800 4200 2    50   Input ~ 0
GP18
Text GLabel 5800 4100 2    50   Input ~ 0
GP19
Text GLabel 5800 4000 2    50   Input ~ 0
GP20
Text GLabel 5800 3900 2    50   Input ~ 0
GP21
Text GLabel 5800 3800 2    50   Input ~ 0
GND
Text GLabel 5800 3700 2    50   Input ~ 0
GP22
Text GLabel 5800 3200 2    50   Input ~ 0
GP28
Text GLabel 5800 3300 2    50   Input ~ 0
GND
Text GLabel 5800 3600 2    50   Input ~ 0
RUN
Text GLabel 5800 3500 2    50   Input ~ 0
GP26
Text GLabel 5800 3400 2    50   Input ~ 0
GP27
Text GLabel 2600 4300 0    50   Input ~ 0
GP16
$EndSCHEMATC
